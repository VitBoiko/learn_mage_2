<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Unit1\PluginModule\Plugin\Model;


use Magento\Catalog\Model\Product;

class PriceSmile
{
    public function afterGetPrice(Product $product, $result)
    {
        return $result . ' :-)';
    }
}