<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Unit1\PluginModule\Plugin\Block;


use Magento\Theme\Block\Html\Breadcrumbs;

class CustomizedBreadcrumbs
{
    public function beforeAddCrumb(Breadcrumbs $breadcrumbs, $crumb_name, $crumb_info){
        return [
            $crumb_name . "(!)",
            $crumb_info
        ];
    }
}