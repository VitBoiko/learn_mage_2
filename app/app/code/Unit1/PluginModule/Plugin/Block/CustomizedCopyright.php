<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Unit1\PluginModule\Plugin\Block;


use Magento\Theme\Block\Html\Footer;

class CustomizedCopyright
{
    public function aroundGetCopyright(Footer $footer, callable $method)
    {
        return 'Customized copyright!';
    }
}