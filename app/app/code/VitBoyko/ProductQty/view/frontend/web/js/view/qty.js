define(
    [
        'jquery',
        'ko',
        'uiComponent'
    ],
    function ($, ko, Component) {
        'use strict';
        
        return Component.extend(
            {
                defaults: {
                    //html ko template
                    template: 'VitBoyko_ProductQty/addtocart'
                },
                
                initialize: function () {
                    this._super();
                    this.qty = ko.observable(this.defaultQty);
                },

                decreaseQty: function () {
                    var newQty = parseInt(this.qty()) - 1;
                    if(newQty < 1){
                        newQty = 1;
                    }
                    this.qty(newQty);
                    $('input[name="qty"]').val(newQty);
                },

                increaseQty: function () {
                    var newQty = parseInt(this.qty()) + 1;
                    if(newQty > 100){
                        newQty = 100;
                    }
                    this.qty(newQty);
                    $('input[name="qty"]').val(newQty);
                },

                qtyChanged: function () {
                    var input  = $('input[name="qty"]');
                    var newQty = parseInt(input.val());
                    if(isNaN(newQty)){
                        input.val(1);
                        newQty = 1;
                    }
                    if(newQty > 100){
                        newQty = 100;
                    }
                    this.qty(parseInt(newQty));
                    input.val(newQty);
                }
            }
        )
    }
);