<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Test\Model;

use Training\Test\Model\Config\Reader;

class Config extends \Magento\Framework\Config\Data implements
    \Training\Test\Model\Config\ConfigInterface
{
    public function __construct(
        Reader $reader,
        \Magento\Framework\Config\CacheInterface $cache,
        $cacheId = 'training_test_config'
    ) {
        parent::__construct($reader, $cache, $cacheId);
    }

    public function getMyNodeInfo() {
        return $this->get();
    }
}