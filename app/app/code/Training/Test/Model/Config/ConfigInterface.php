<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Test\Model\Config;


interface ConfigInterface
{
    public function getMyNodeInfo();
}