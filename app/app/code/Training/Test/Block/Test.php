<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Test\Block;


class Test extends \Magento\Framework\View\Element\AbstractBlock
{

    protected $storeCollection;
    /**
     * @var \Magento\Store\Model\ResourceModel\Group\Collection
     */
    private $groupCollection;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Store\Model\ResourceModel\Store\Collection $storeCollection,
        \Magento\Store\Model\ResourceModel\Group\Collection $groupCollection,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeCollection = $storeCollection;
        $this->groupCollection = $groupCollection;
    }

    protected function _toHtml()
    {
        $listOfStores = $this->storeCollection->getItems();
        $result = [];
        foreach ($listOfStores as $store) {
            $rootCategoryId = $store->getRootCategoryId();
            $this->groupCollection->addFieldToFilter('root_category_id', $rootCategoryId);
            $categoryCollection = $this->groupCollection->getItems();
            foreach ($categoryCollection as $category) {
                $result[] = [
                    $category->getName(),
                    $store->getName()
                    ];
            }
        }

        print_r($result);
    }
}
