<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Test\Controller\Action;


use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Context;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepositoryInterface;


    /**
     * Index constructor.
     * @param Context $context
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param FilterBuilder $filterBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepositoryInterface,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        parent::__construct($context);
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepositoryInterface = $productRepositoryInterface;
    }

    public function execute()
    {
        $filter = $this->filterBuilder
            ->setField('material')
            ->setValue(14)
            ->setConditionType('finset')
            ->create();
        $this->searchCriteriaBuilder->addFilters([$filter]);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $products = $this->productRepositoryInterface->getList($searchCriteria)->getItems();

        foreach ($products as $product) {
            echo $product->getId() . ',';
        }

    }
}
