<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Test\Controller\Action;


use Magento\Framework\App\Action\Context;
use Training\Test\Model\Config\ConfigInterface;

class Config extends \Magento\Framework\App\Action\Action
{
    protected $testConfig;

    public function __construct(Context $context, ConfigInterface $config)
    {
        parent::__construct($context);
        $this->testConfig = $config;
    }

    public function execute()
    {
        $testConfig = $this->testConfig;
        $myNodeInfo = $testConfig->getMyNodeInfo();
        if (is_array($myNodeInfo)) {
            foreach ($myNodeInfo as $str) {
                $this->getResponse()->appendBody($str . "<BR>");
            }
        }
    }
}