<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Test\Plugin\Adminhtml\Product;


use Psr\Log\LoggerInterface;

class LogProductSave
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * LogProductSave constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResource
     * @param \Magento\Framework\Model\AbstractModel $product
     */
    public function beforeSave(
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Magento\Framework\Model\AbstractModel $product
    ) {
        $origData = $product->getOrigData();
        $data = $product->getData();
        $changedData[] = $product->getId();
        foreach ($origData as $key => $value) {
            if ($data[$key] != $value) {
                $changedData[] = [$key => $data[$key]];
            }
        }

        $message = print_r($changedData, true);
        $this->logger->info($message);
    }
}
