<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Test\Plugin\Controller\Product;

class View
{
    /**
     * @var \Magento\Framework\View\Layout
     */
    protected $layout;
    /**
     * @var \Magento\Framework\Controller\Result\Raw
     */
    private $resultRaw;

    /**
     * View constructor.
     * @param \Magento\Framework\View\Layout $layout
     * @param \Magento\Framework\Controller\Result\Raw $resultRaw
     */
    public function __construct(
        \Magento\Framework\View\Layout $layout,
        \Magento\Framework\Controller\Result\Raw $resultRaw
    ) {
        $this->layout = $layout;
        $this->resultRaw = $resultRaw;
    }

    /**
     * @param \Magento\Catalog\Controller\Product\View $controller
     * @param $result
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function afterExecute(\Magento\Catalog\Controller\Product\View $controller, $result)
    {
        $xml =  $this->layout->generateXml();
        $result = $this->resultRaw->setHeader('Content-Type','text/xml')
            ->setContents($xml->getXmlString());

        return $result;
    }
}