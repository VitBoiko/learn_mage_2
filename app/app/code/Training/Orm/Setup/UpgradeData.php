<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Orm\Setup;


use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as CatalogAttribute;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CategorySetupFactory
     */
    protected $catalogSetupFactory;
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;
    /**
     * @var AttributeSetFactory
     */
    private $setFactory;

    public function __construct(
        CategorySetupFactory $categorySetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        StoreManagerInterface $storeManager,
        AttributeRepositoryInterface $attributeRepository,
        AttributeSetFactory $setFactory
    ) {
        $this->catalogSetupFactory = $categorySetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->storeManager = $storeManager;
        $this->attributeRepository = $attributeRepository;
        $this->setFactory = $setFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.1', '<')) {
            /** @var CategorySetup $catalogSetup */
            $catalogSetup = $this->catalogSetupFactory->create(['setup' => $setup]);
            $catalogSetup->addAttribute(Product::ENTITY, 'multiselect_attr', [
                'label' => 'Multiselect',
                'visible_on_front' => 1,
                'required' => 0,
                'global' => CatalogAttribute::SCOPE_STORE,
                'type' => 'text',
                'user_defined' => 1,
                'input' => 'multiselect',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'option' => [
                    'values' => [
                        'option_1',
                        'option_2',
                        'option_3',
                        'option_4'
                    ]
                ],
            ]);

            $catalogSetup->addAttributeToGroup(
                ProductAttributeInterface::ENTITY_TYPE_CODE,
                'Default',
                'Product Details',
                'multiselect_attr',
                86
            );
        }

        if (version_compare($context->getVersion(), '0.1.2', '<')) {
            /** @var CategorySetup $catalogSetup */
            $catalogSetup = $this->catalogSetupFactory->create(['setup' => $setup]);
            $catalogSetup->updateAttribute(
                Product::ENTITY,
                'multiselect_attr',
                [
                    'frontend_model' => '\Training\Orm\Entity\Attribute\Frontend\HtmlList',
                    'is_html_allowed_on_front' => 1,
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.1.3', '<')) {
            /** @var CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerSetup->addAttribute(
                Customer::ENTITY,
                'priority',
                [
                    'label' => 'Priority',
                    'type' => 'int',
                    'input' => 'select',
                    'source' => '\Training\Orm\Entity\Attribute\Source\CustomerPriority',
                    'required' => 0,
                    'system' => 0,
                    'position' => 100
                ]
            );
            $customerEntity = $customerSetup->getEavConfig()
                ->getEntityType('customer');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();

            $attributeSet = $this->setFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
            $priorityAttribute = $customerSetup->getEavConfig()
                ->getAttribute(Customer::ENTITY, 'priority');

            $priorityAttribute->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer'],
            ]);

            $this->attributeRepository->save($priorityAttribute);
        }

        $setup->endSetup();
    }
}