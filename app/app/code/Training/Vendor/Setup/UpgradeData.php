<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

namespace Training\Vendor\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();


        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            $configTable = $setup->getTable('training_vendor_entity');
            $setup->getConnection()->insert(
                $configTable,
                [   'training_vendor' => 1,
                    'training_vendor_new' => 1
                ]
            );
        }

        $setup->endSetup();
    }
}
