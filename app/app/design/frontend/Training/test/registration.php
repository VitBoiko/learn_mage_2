<?php
/**
 * @author Vitalii Boyko <vitaliyboyko@i.ua>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Training/test',
    __DIR__
);
